public class MathTest {
	
	public static void main(String[] args) {
		int num1 = 12;
		int num2 = 3;
		
		//int res ;
		
		System.out.println("sum of two numbers:"+add(num1,num2));
		
		System.out.println("subtraction of two numbers:"+sub(num1,num2));
		
	}
	
	public static int add(int num1, int num2){
		return (num1+num2);
	}
	
	public static int sub(int num1, int num2){
		return (num1-num2);
	}
	
	public static int multiply(int num1, int num2){
		return (num1*num2);
	}
	
	public static int divide(int num1, int num2){
		return (num1/num2);
	}

}
